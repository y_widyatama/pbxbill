CREATE TABLE `users` (
  `id` varchar(24) NOT NULL,
  `name` varchar(125) DEFAULT NULL,
  `pwdhash` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;