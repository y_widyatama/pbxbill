-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: pbxbill
-- ------------------------------------------------------
-- Server version	5.1.35-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `pbxbill`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `pbxbill` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pbxbill`;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `trunkcode` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratingclass`
--

DROP TABLE IF EXISTS `ratingclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratingclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destinationnet` varchar(100) DEFAULT NULL,
  `code` varchar(16) DEFAULT NULL,
  `rateusd` decimal(10,5) DEFAULT NULL,
  `wholesalehkd` decimal(10,5) DEFAULT NULL,
  `retailhkd` decimal(10,5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idxcode` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=30894 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tvoice`
--

DROP TABLE IF EXISTS `tvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tvoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `networkname` varchar(45) DEFAULT NULL,
  `totalcharge` decimal(10,4) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `originatingtrunkgroupclli` varchar(45) DEFAULT NULL,
  `ratingclass_id` int(11) DEFAULT NULL,
  `totalduration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voice_cdr`
--

DROP TABLE IF EXISTS `voice_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voice_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `originating_number` varchar(45) DEFAULT NULL,
  `destination_number` varchar(45) DEFAULT NULL,
  `origtrunkgroupclli` varchar(45) DEFAULT NULL,
  `origtrunkgroup` varchar(45) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `rate` decimal(10,4) DEFAULT NULL,
  `ratingclass_id` int(11) DEFAULT NULL,
  `ratedcharge` decimal(10,4) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `stop` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating` (`ratingclass_id`),
  KEY `fk_cus` (`cust_id`),
  CONSTRAINT `fk_cus` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating` FOREIGN KEY (`ratingclass_id`) REFERENCES `ratingclass` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7761 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-12 12:43:41
