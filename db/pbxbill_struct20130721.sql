CREATE DATABASE  IF NOT EXISTS `pbxbill` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pbxbill`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: pbxbill
-- ------------------------------------------------------
-- Server version	5.1.35-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `trunkcode` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `activecontract_id` int(11) DEFAULT NULL,
  `invoicecode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_firstcontract` (`activecontract_id`),
  CONSTRAINT `fk_firstcontract` FOREIGN KEY (`activecontract_id`) REFERENCES `contract` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bankinfo`
--

DROP TABLE IF EXISTS `bankinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(100) DEFAULT NULL,
  `bankdetail` varchar(100) DEFAULT NULL,
  `account` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `swiftcode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tvoice`
--

DROP TABLE IF EXISTS `tvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tvoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `networkname` varchar(45) DEFAULT NULL,
  `totalcharge` decimal(10,4) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `originatingtrunkgroupclli` varchar(45) DEFAULT NULL,
  `ratingclass_id` int(11) DEFAULT NULL,
  `totalduration` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cust2` (`cust_id`),
  CONSTRAINT `fk_cust2` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=470 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratingclass`
--

DROP TABLE IF EXISTS `ratingclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratingclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destinationnet` varchar(100) DEFAULT NULL,
  `code` varchar(16) DEFAULT NULL,
  `rateusd` decimal(10,5) DEFAULT NULL,
  `wholesalehkd` decimal(10,5) DEFAULT NULL,
  `retailhkd` decimal(10,5) DEFAULT NULL,
  `tariffschema_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idxcode` (`tariffschema_id`,`code`),
  KEY `fk_ratingclass2schema` (`tariffschema_id`),
  CONSTRAINT `fk_ratingclass2schema` FOREIGN KEY (`tariffschema_id`) REFERENCES `tariff_schema` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61812 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tariff_schema`
--

DROP TABLE IF EXISTS `tariff_schema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tariff_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `dateref` date DEFAULT NULL,
  `info` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractno` varchar(45) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `contractdate` date DEFAULT NULL,
  `contractend` date DEFAULT NULL,
  `startperiod` int(11) DEFAULT NULL,
  `schema_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contract2schema` (`schema_id`),
  CONSTRAINT `fk_contract2schema` FOREIGN KEY (`schema_id`) REFERENCES `tariff_schema` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceno` varchar(45) DEFAULT NULL,
  `invoicedate` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `invoiceamount` decimal(9,2) DEFAULT NULL,
  `activecontract_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cust` (`cust_id`),
  CONSTRAINT `fk_cust` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voice_cdr`
--

DROP TABLE IF EXISTS `voice_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voice_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `originating_number` varchar(45) DEFAULT NULL,
  `destination_number` varchar(45) DEFAULT NULL,
  `origtrunkgroupclli` varchar(45) DEFAULT NULL,
  `origtrunkgroup` varchar(45) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `rate` decimal(10,4) DEFAULT NULL,
  `ratingclass_id` int(11) DEFAULT NULL,
  `ratedcharge` decimal(10,4) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `stop` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating` (`ratingclass_id`),
  KEY `fk_cus` (`cust_id`),
  CONSTRAINT `fk_cus` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating` FOREIGN KEY (`ratingclass_id`) REFERENCES `ratingclass` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7845 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-21 18:15:19
