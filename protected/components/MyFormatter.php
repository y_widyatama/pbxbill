<?php
/**
 * Created by JetBrains PhpStorm.
 * User: yudhi
 * Date: 7/7/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
class MyFormatter extends CFormatter
{
    public $currencyPrefix;
    public function setDecimals($dec) {
        $nf =$this->numberFormat;
        $nf['decimals']=$dec;
        $this->numberFormat = $nf;
        return $this;
    }
    public function __construct($currency = '')
    {
        $this->currencyPrefix = "HK$";
        if (!empty($currency))
            $this->currencyPrefix  = $currency;
        $nf =$this->numberFormat;
        $nf['decimals']=2;
        $this->numberFormat = $nf;

    }

    public function formatCurrency($v)
    {
        return $this->currencyPrefix . parent::formatNumber($v);
    }
    public function formatCent($v)
    {
        return parent::formatNumber($v * 0.01);
    }
    public static function minsecs($secs)
    {
        $mins = floor($secs / 60.0);
        $secs = $secs - ($mins*60);
        $nf = Yii::app()->numberFormatter;
        return $nf->format('##00',$mins).':'.$nf->format('00',$secs);
    }
}
