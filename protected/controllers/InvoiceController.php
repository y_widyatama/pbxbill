<?php

class InvoiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','preview','view','index','viewPdf','viewPdfRev','viewRev','downloadCsv','genPdfRev'),
				'users'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','preview','view','index','viewPdf', 'create','update','viewPdfRev','viewRev','genPdfRev'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionViewRev($id)
	{
		$this->layout = 'invoice1';
        $this->pageTitle = 'INVOICE';
    	$this->render('viewInvoiceRev',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionPreview($id)
    {
           $this->layout = 'invoice1';
        $this->pageTitle = 'INVOICE';
    		$this->render('viewInvoice',array(
    			'model'=>$this->loadModel($id),
    		));
    }
	
	public function actionDownloadCsv($id,$format=1)
	{
		$model = $this->loadModel($id);
		$period = $model->period; $cust_id = $model->cust_id;
		$c = new CDbCriteria();
		$c->order = 'start';
		$c->compare('batch_id',$period);
		$c->compare('cust_id',$cust_id);
		if ($format != 3)
			$c->compare('ratedcharge','>0');
		$rows = VoiceCdr::model()->findAll($c);
		$filename = 'invoicedetail.csv';

	  	header("Content-Type: text/csv; charset=utf-8");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename ");
		header("Content-Transfer-Encoding: binary ");
		$S = ",";
		/** @property integer $batch_id
 * @property integer $cust_id
 * @property string $originating_number
 * @property string $destination_number
 * @property string $origtrunkgroupclli
 * @property string $origtrunkgroup
 * @property integer $duration
 * @property string $rate
 * @property integer $ratingclass_id
 * @property string $ratedcharge
 * @property string $start
 * @property string $stop
 */
 /*
 		'start','stop','originating_number',array('name'=>'durationR','cssClassExpression'=>'"rightalign"'),
		array('name'=>'rate','type'=>'number','cssClassExpression'=>'"rightalign"','header' =>'Rate ('.$currency.'/min)'),
        array('name'=>'ratedcharge','type'=>'number','cssClassExpression'=>'"rightalign"', 'header'=>'Charge ('.$currency.')'),

 */
		if ($format == 1 || $format == 3)
		echo "batch_id $S cust_id $S originating $S destination $S origtrunkgroupclli $S origtrunkgroup $S duration $S rate $S".
        "\"Rated charge\"$S start $S stop $S \r\n";
		else
		echo "start $S stop $S originating $S duration $S rate $S ratedcharge \r\n";
		if ($format==1 || $format == 3)
		foreach ($rows as $row)
		{
            /**
             * @var VoiceCdr $row
             */
			
		   echo $row->batch_id;
		   echo $S;
		   echo $row->cust_id;
		   echo $S;
		   echo '"'.$row->originating_number.'"';
		   echo $S;
		   echo '"'.$row->destination_number.'"';
		   echo $S;
           echo $row->origtrunkgroupclli;
           echo $S;
		   echo $row->origtrunkgroup;
		   echo $S;
		   echo $row->duration*0.01;
		   echo $S;
           echo $row->rate;
           echo $S;
           echo $row->ratedcharge*1;
            echo $S;
            echo $row->start;
            echo $S;
            echo $row->stop;
		   echo "\r\n";
		}
		else 
		foreach ($rows as $row)
		{
            /**
             * @var VoiceCdr $row
             */
			
		   echo $row->start;
		   echo $S;
		   echo $row->stop;
		   echo $S;
		   echo '"'.$row->originating_number.'"';
		   echo $S;
		   echo $row->durationR;
		   echo $S;
           echo $row->rate;
           echo $S;
           echo $row->ratedcharge*1;
		   echo "\r\n";
		}
		exit;

	}
	
 
    public function actionViewPdf($id,$tofile=0)
    { 
	set_time_limit(0);
        $this->layout = 'invoice1';
        $this->pageTitle = 'INVOICE';
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->SetFooter("|Page {PAGENO}|");
        $model = $this->loadModel($id);
   		$mPDF1->WriteHTML(

    		$this->render('viewInvoice',array(
    			'model'=>$model,
    		),true));
        if (!$tofile)	
			$mPDF1->Output();
		else {
		    $mPDF1->Output("output/inv$id.pdf","F");
			$this->redirect("output/inv$id.pdf");
		}

    }

	public function actionViewPdfRev($id,$tofile=0)
    {
		set_time_limit(0);
        $this->layout = 'invoice1';
        $this->pageTitle = 'INVOICE';
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->SetFooter("|Page {PAGENO}|");
        $model = $this->loadModel($id);
   		$mPDF1->WriteHTML(

    		$this->render('viewInvoiceRev',array(
    			'model'=>$model,
    		),true));
		if (!$tofile)	
			$mPDF1->Output();
		else {
		    $mPDF1->Output("output/inv$id.pdf","F");
			$this->redirect("output/inv$id.pdf");
		}

    }

    public function actionGenPdfRev($idStart,$idEnd,$tofile=0)
    {
		set_time_limit(0);
        for ($id = $idStart; $id<=$idEnd; $id++)
        {
            $this->layout = 'invoice1';
            $this->pageTitle = 'INVOICE';
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            $mPDF1->SetFooter("|Page {PAGENO}|");
            $model = $this->loadModel($id);
            $mPDF1->WriteHTML(

                $this->render('viewInvoiceRev',array(
                    'model'=>$model,
                ),true));
            if (!$tofile)	
                $mPDF1->Output();
            else {
                $mPDF1->Output("output/inv$id.pdf","F");
            }
		}
        $this->redirect("output/inv$id.pdf");
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Invoice;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Invoice');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invoice('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invoice']))
			$model->attributes=$_GET['Invoice'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Invoice the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		try
		{
			$model=Invoice::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		catch(CDbException $e)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;	
	}

	/**
	 * Performs the AJAX validation.
	 * @param Invoice $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invoice-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
