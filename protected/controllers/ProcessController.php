<?php

class ProcessController extends Controller
{
	var $layout = 'column2';
	public function actionClearRating()
	{
		$model = new PeriodForm();
		$msg = '';
		if (isset($_POST['PeriodForm'])) 
		{
		   $model->attributes = $_POST['PeriodForm'];
		   VoiceCdr::clearRating($model->batch_id);
		   $msg = 'Cleared rating in period '.$model->batch_id;
		}
		$this->render('clearRating',compact('model','msg'));
	}

	public function actionDoInvoice()
	{
		$model = new PeriodForm();
		$msg = '';
		if (isset($_POST['PeriodForm'])) 
		{
		   $model->attributes = $_POST['PeriodForm'];
		   VoiceCdr::batchInvoice($model->batch_id,$model->cust_id);
		   $msg = 'Processed invoicing for period '.$model->batch_id.' '.($model->cust_id ? 'cust '.$model->cust_id : '');
		}
		$this->render('doInvoice',compact('model','msg'));
	}

    public function actionDoMirror()
	{
		set_time_limit(0);
		$model = new PeriodForm();
		$msg = '';
		if (isset($_GET['page']))
		{
		   $model->batch_id = $_GET['batch_id'];
		   $page = intval($_GET['page']);
		   $m = VoiceCdr::batchMirror($model->batch_id,$page);

		   $msg = 'Processed mirroring for period '.$model->batch_id. $m;
		}
		if (isset($_POST['PeriodForm'])) 
		{
		   $model->attributes = $_POST['PeriodForm'];
		   $m = VoiceCdr::batchMirror($model->batch_id);
		   
		   $msg = 'Processed mirroring for period '.$model->batch_id . $m;
		}
		$this->render('doMirror',compact('model','msg'));
	}
    
	public function actionDoRating()
	{
		set_time_limit(0);
		$model = new PeriodForm();
		$msg = '';
		if (isset($_GET['page']))
		{
		   $model->batch_id = $_GET['batch_id'];
		   $page = intval($_GET['page']);
		   $cnt = VoiceCdr::batchRating($model->batch_id,$page);
		   if ($cnt != 0)
		   {
		      $this->redirect(array('process/doRating','batch_id'=>$model->batch_id,'page'=>$page+1));
			  return;
		   }
		   $msg = 'Processed rating for period '.$model->batch_id;
		}
		if (isset($_POST['PeriodForm'])) 
		{
		   $model->attributes = $_POST['PeriodForm'];
		   $cnt = VoiceCdr::batchRating($model->batch_id,-1);
		   if ($cnt != 0)
		   {
		      $this->redirect(array('process/doRating','batch_id'=>$model->batch_id,'page'=>1));
			  return;
		   }
		   $msg = 'Processed rating for period '.$model->batch_id;
		}
		$this->render('doRating',compact('model','msg'));
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
