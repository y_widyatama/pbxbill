<?php

/**
 * This is the model class for table "bankinfo".
 *
 * The followings are the available columns in table 'bankinfo':
 * @property integer $id
 * @property string $companyname
 * @property string $bankdetail
 * @property string $account
 * @property string $address
 * @property string $swiftcode
 */
class Bankinfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bankinfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bankinfo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyname, bankdetail', 'length', 'max'=>100),
			array('account, address, swiftcode', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, companyname, bankdetail, account, address, swiftcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'companyname' => 'Company Name',
			'bankdetail' => 'Bank Detail',
			'account' => 'Account No.',
			'address' => 'Address',
			'swiftcode' => 'Swift Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('companyname',$this->companyname,true);
		$criteria->compare('bankdetail',$this->bankdetail,true);
		$criteria->compare('account',$this->account,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('swiftcode',$this->swiftcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}