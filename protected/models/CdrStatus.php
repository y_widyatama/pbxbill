<?php

/**
 * This is the model class for table "cdrstatus".
 *
 * The followings are the available columns in table 'cdrstatus':
 * @property integer $id
 * @property integer $is_processed
 * @property string $process_time
 * @property integer $process_cnt
 */
class CdrStatus extends CActiveRecord
{
    static $ftpConn = null;
    public static function batchArchive($fromId,$toId)
    {
        $c = new CDbCriteria();
        $c->addBetweenCondition('id',$fromId,$toId);
        $batchList = CdrStatus::model()->findAll($c);
        foreach ($batchList as $row)
            $row->uploadArchive(false);

    }
    public static function connectFtp()
    {
        if (!empty(CdrStatus::$ftpConn)) return CdrStatus::$ftpConn;
        $conn_id=ftp_connect(Yii::app()->params['archiveHost']);
		$login = ftp_login($conn_id,Yii::app()->params['archiveUser'],Yii::app()->params['archivePwd']);
        if (!$login) throw new CHttpException(500,"Unable to login to Archive server");
        ftp_pasv($conn_id,TRUE);
        CdrStatus::$ftpConn = $conn_id;
        return $conn_id;
    }
    public function getArchiveSize()
    {
        $prefix1 = '/a2pdata/pbxbill/CDR/DONE/';
        $prefix2 = '/a2pdata/pbxbill/CDR/DONE2015/';
        $prefix3 = '/a2pdata/pbxbill/CDR/DONE2016/';
		$prefix4 = '/a2pdata/pbxbill/CDR/DONE2017/';
		$prefix5 = '/a2pdata/pbxbill/CDR/DONE2018/';
		$prefix6 = '/a2pdata/pbxbill/CDR/DONE2019/';
        $fname = 'HKVCDR.'.$this->id;
        $conn = CdrStatus::connectFtp();
        $n = 0;
        $n = @ftp_size($conn,$prefix1.$fname);
        if ($n>0) return $n;
        $n = @ftp_size($conn,$prefix2.$fname);
        if ($n>0) return $n;
        $n = @ftp_size($conn,$prefix3.$fname);
        if ($n>0) return $n;
        $n = @ftp_size($conn,$prefix4.$fname);
		if ($n>0) return $n;
		$n = @ftp_size($conn,$prefix5.$fname);
		if ($n>0) return $n;
		$n = @ftp_size($conn,$prefix6.$fname);
        if ($n>0) return $prefix6.$fname;
        
        return 0;
    }
    public function getArchiveName()
    {
        $prefix1 = '/a2pdata/pbxbill/CDR/DONE/';
        $prefix2 = '/a2pdata/pbxbill/CDR/DONE2015/';
        $prefix3 = '/a2pdata/pbxbill/CDR/DONE2016/';
        $prefix4 = '/a2pdata/pbxbill/CDR/DONE2017/';
        $prefix5 = '/a2pdata/pbxbill/CDR/DONE2018/';
        $prefix6 = '/a2pdata/pbxbill/CDR/DONE2019/';
        $fname = 'HKVCDR.'.$this->id;
        $conn = CdrStatus::connectFtp();
        $n = 0;
        $n = @ftp_size($conn,$prefix1.$fname);
        if ($n>0) return $prefix1.$fname;
        $n = @ftp_size($conn,$prefix2.$fname);
        if ($n>0) return $prefix2.$fname;
        $n = @ftp_size($conn,$prefix3.$fname);
        if ($n>0) return $prefix3.$fname;
        $n = @ftp_size($conn,$prefix4.$fname);
		if ($n>0) return $prefix4.$fname;
		$n = @ftp_size($conn,$prefix5.$fname);
        if ($n>0) return $prefix5.$fname;
        $n = @ftp_size($conn,$prefix6.$fname);
        if ($n>0) return $prefix6.$fname;
        
        return 0;
    }
    public function uploadArchive($interactive=true)
    {
		if ((!$interactive) && ($this->is_processed>=10)) return;
		$year = date('Y');
		$newdone = "DONE$year";
		$prefix1 = "/home/pbxbill/CDR/$newdone/";
		if (!is_dir($prefix1)) mkdir($prefix1);
		$prefixSvr = "/a2pdata/pbxbill/CDR/DONE$year/";
        $localLoc = '/home/pbxbill/CDR/AUTOPROCESS/';
		$conn = CdrStatus::connectFtp();
		$directories = @ftp_nlist($conn,"/a2pdata/pbxbill/CDR");
		if (!in_array($newdone,$directories))
		   @ftp_mkdir($conn,$prefixSvr);
        $fname = 'HKVCDR.'.$this->id;
        $fullname = $localLoc.$fname;
        $localsize = 0;
        $localsize = @filesize($fullname);
        $remotesize = 0;
        $remotesize = @ftp_size($conn,$prefixSvr.$fname);
		$doUpload = false;
        if (($localsize>0) && ($remotesize<=0)) $doUpload=true;
        if (($localsize>0) && ($remotesize<$localsize)) $doUpload=true;
        if ($doUpload)
        {
			if ($remotesize>0) ftp_delete($conn,$prefixSvr.$fname);
            $res = ftp_put($conn,$prefixSvr.$fname,$fullname,FTP_BINARY);
            if (!$res) throw new CHttpException(500,"Unable to upload file to ftp server");
        }
        $remotesize = @ftp_size($conn,$prefixSvr.$fname);
        if ($remotesize == $localsize)
        {
			$out = "";
        	exec("pbzip2 ".$fullname,$out);
			$out = var_export($out,true);
			if ($interactive) echo "<br>pbzip2 result is <pre>".$out."</pre><br>";
		}
		$isOk=false;
		if (file_exists($fullname.".bz2") && (@filesize($fullname.".bz2")>0))
		{
        	exec("mv ".$fullname.".bz2 ".$prefix1);
			$this->is_processed = 10+$this->is_processed;
			$this->save();
			$isOk=true;
        }
        if (!$isOk) if ($interactive) throw new CHttpException(500, "Unable to do archiving: localsize ".$localsize." remote size ".$remotesize);
    }
	/**
	 * Returns the static model of the specified AR class.
	 * @return CdrStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cdrstatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, is_processed, process_cnt', 'numerical', 'integerOnly'=>true),
			array('process_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, is_processed, process_time, process_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'is_processed' => 'Is Processed',
			'process_time' => 'Process Time',
			'process_cnt' => 'Process Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('is_processed',$this->is_processed);

		$criteria->compare('process_time',$this->process_time,true);

		$criteria->compare('process_cnt',$this->process_cnt);

		return new CActiveDataProvider('CdrStatus', array(
			'criteria'=>$criteria,
		));
	}
}
