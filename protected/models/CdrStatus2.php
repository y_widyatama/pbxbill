<?php

/**
 * This is the model class for table "cdrstatus2".
 *
 * The followings are the available columns in table 'cdrstatus':
 * @property integer $id
 * @property integer $is_processed
 * @property string $process_time
 * @property integer $process_cnt
 */
class CdrStatus2 extends CActiveRecord
{
    static $ftpConn = null;
    public static function batchArchive($fromId,$toId)
    {
        $c = new CDbCriteria();
        $c->addBetweenCondition('id',$fromId,$toId);
        $batchList = CdrStatus2::model()->findAll($c);
        foreach ($batchList as $row)
            $row->uploadArchive(false);

    }
    public static function connectFtp()
    {
		if (!empty(CdrStatus2::$ftpConn)) return CdrStatus2::$ftpConn;
		$port = 21;
        if (!empty(Yii::app()->params['archivePort'])) $port =  intval(Yii::app()->params['archivePort']);
        $conn_id=ftp_connect(Yii::app()->params['archiveHost'],$port);
		$login = ftp_login($conn_id,Yii::app()->params['archiveUser'],Yii::app()->params['archivePwd']);
        if (!$login) throw new CHttpException(500,"Unable to login to Archive server");
        ftp_pasv($conn_id,TRUE);
        CdrStatus2::$ftpConn = $conn_id;
        return $conn_id;
    }
    public function getArchiveSize()
    {
        $prefix1 = '/a2pdata/pbxbill/CDR2/DONE/';
        $prefix2 = '/a2pdata/pbxbill/CDR2/DONE2019/';
		$padId = str_pad(''.$this->id,6,"0",STR_PAD_LEFT);
		$conn = CdrStatus2::connectFtp();
		$n = 0;
		$filenames1 = @ftp_nlist($conn,$prefix1.'sg2_cdr_*_'.$padId.'.CDR*');
		if ($filenames1== false) $filenames1 = array();
		$filenames2 = @ftp_nlist($conn,$prefix2.'sg2_cdr_*_'.$padId.'.CDR*');
		if ($filenames2== false) $filenames2 = array();
		$filenames = array_merge($filenames1,$filenames2);
		if (count($filenames)>0)
		{
			$filename = $filenames[0];
			$n = @ftp_size($conn,$filename);
		} else $n = 0;
		return $n;
    }
    public function getArchiveName()
    {
		$prefix1 = '/a2pdata/pbxbill/CDR2/DONE/';
        $prefix2 = '/a2pdata/pbxbill/CDR2/DONE2019/';
		$fname = 'HKVCDR.'.$this->id;
		$padId = str_pad(''.$this->id,6,"0",STR_PAD_LEFT);
		$conn = CdrStatus2::connectFtp();
		$filenames1 = @ftp_nlist($conn,$prefix1.'sg2_cdr_*_'.$padId.'.CDR*');
		if ($filenames1== false) $filenames1 = array();
		$filenames2 = @ftp_nlist($conn,$prefix2.'sg2_cdr_*_'.$padId.'.CDR*');
		if ($filenames2== false) $filenames2 = array();
		$filenames = array_merge($filenames1,$filenames2);
		if (count($filenames)>0)
		{
			$filename = $filenames[0];
			return $filename;
		}
		else return "-";
	}
	function endsWith($string, $endString) 
	{ 
		$len = strlen($endString); 
		if ($len == 0) { 
			return true; 
		} 
		return (substr($string, -$len) === $endString); 
	} 
    public function uploadArchive($interactive=true)
    {
		if ((!$interactive) && ($this->is_processed>=10)) return;
		$year = date('Y');

		$newdone = "DONE$year";
		$prefix1 = "/home/pbxbill/CDR/$newdone/";
		if (!is_dir($prefix1)) mkdir($prefix1);
		$prefixSvr = "/a2pdata/pbxbill/CDR2/DONE$year/";
        $localLoc = '/home/pbxbill/CDR/AUTOPROCESS/';
		$conn = CdrStatus2::connectFtp();
		$directories = @ftp_nlist($conn,"/a2pdata/pbxbill/CDR2");
		if (!in_array($newdone,$directories))
		   @ftp_mkdir($conn,$prefixSvr);
		$oldDir = getcwd();
		chdir ($localLoc);
		$padId = str_pad(''.$this->id,6,"0",STR_PAD_LEFT);
		$globResult = glob("sg2_cdr_*_$padId.CDR");
		if (count($globResult) == 0)
			$globResult = glob("sg2_cdr_*_$padId.CDR.bz2");
		chdir ($oldDir);
		$localsize = 0;
        $remotesize = 0;
        
		if (count($globResult)>0)
		{
			$fname = $globResult[0];
			$fullname = $localLoc.$fname;
			$localsize = @filesize($fullname);
			$remotesize = @ftp_size($conn,$prefixSvr.$fname);
			if ($interactive)  echo "<br>filename is <pre>".$fname."</pre> fullpath is <pre>".$fullname."</pre><br>";	
		} else {
			$fullname = "localfile-not-found";
		}
        $doUpload = false;
        if (($localsize>0) && ($remotesize<=0)) $doUpload=true;
        if (($localsize>0) && ($remotesize<$localsize)) $doUpload=true;
        if ($doUpload)
        {
			if ($remotesize>0) ftp_delete($conn,$prefixSvr.$fname);
            $res = ftp_put($conn,$prefixSvr.$fname,$fullname,FTP_BINARY);
			if (!$res) throw new CHttpException(500,"Unable to upload file to ftp server");
			$remotesize = @ftp_size($conn,$prefixSvr.$fname);
        }
        if (($remotesize == $localsize)&&($localsize>0))
        {
			$out = "";
			if (!$this->endsWith($fullname,".bz2"))
        	exec("pbzip2 ".$fullname,$out);
			$out = var_export($out,true);
			if ($interactive) echo "<br>pbzip2 result is <pre>".$out."</pre><br>";
		}
		if ($this->endsWith($fullname,".bz2")) $fullname = substr($fullname,0,strlen($fullname)-4);
		$isOk=false;
		if (file_exists($fullname.".bz2") && (@filesize($fullname.".bz2")>0))
		{
        	exec("mv ".$fullname.".bz2 ".$prefix1,$out);
			$this->is_processed = 10+$this->is_processed;
			$this->save();
			$isOk=true;
			$out = var_export($out,true);
			if ($interactive) echo "<br>mv result is <pre>".$out."</pre><br>";
        }
        if (!$isOk) if ($interactive) throw new CHttpException(500, "Unable to do archiving: localsize ".$localsize." remote size ".$remotesize);
    }
	/**
	 * Returns the static model of the specified AR class.
	 * @return CdrStatus2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cdrstatus2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, is_processed, process_cnt', 'numerical', 'integerOnly'=>true),
			array('process_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, is_processed, process_time, process_cnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'is_processed' => 'Is Processed',
			'process_time' => 'Process Time',
			'process_cnt' => 'Process Cnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('is_processed',$this->is_processed);

		$criteria->compare('process_time',$this->process_time,true);

		$criteria->compare('process_cnt',$this->process_cnt);

		return new CActiveDataProvider('CdrStatus2', array(
			'criteria'=>$criteria,
		));
	}
}
