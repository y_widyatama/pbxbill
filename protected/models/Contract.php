<?php

/**
 * This is the model class for table "contract".
 *
 * The followings are the available columns in table 'contract':
 * @property integer $id
 * @property string $contractno
 * @property integer $cust_id
 * @property integer $schema_id
 * @property integer $startperiod
 * @property string $contractdate
 * @property string $contractend
 * @property TariffSchema $tariffSchema
 */
class Contract extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cust_id,startperiod,schema_id', 'numerical', 'integerOnly'=>true),
			array('contractno', 'length', 'max'=>45),
			array('contractdate, contractend', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contractno, cust_id, contractdate, contractend', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'tariffSchema' => array(self::BELONGS_TO, 'TariffSchema', 'schema_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contractno' => 'Contract No',
			'cust_id' => 'Cust',
			'contractdate' => 'Contract Date',
			'contractend' => 'Contract End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contractno',$this->contractno,true);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('contractdate',$this->contractdate,true);
		$criteria->compare('contractend',$this->contractend,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}