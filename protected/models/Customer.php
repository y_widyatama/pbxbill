<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $address2
 * @property string $trunkcode
 * @property string $phone
 * @property integer $activecontract_id
 * @property string $invoicecode
 * @property string $displayformat
 * @property integer $bank_id
 *
 * The followings are the available model relations:
 * @property Contract $activecontract
 * @property Invoice[] $invoices
 * @property VoiceCdr[] $voiceCdrs
 */
class Customer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}
    static $schemaCache = array();
    public static function getSchemaId($cust_id,$batch_id)
    {
        $key = $cust_id . ':' . $batch_id;
        if (array_key_exists($key, Customer::$schemaCache))
        {
            return Customer::$schemaCache[$key];
        }
        $c = new CDbCriteria();
        $c->order = 'startperiod desc';
        $c->compare('cust_id',$cust_id);
        $c->compare('startperiod','<='.$batch_id);
        $contract = Contract::model()->find($c);
        /**
         * @var Contract $contract
         */
        $schemaId = $contract->schema_id;
        Customer::$schemaCache[$key] = $schemaId;
        return $schemaId;
    }
    public function getContract($batch_id)
    {
        $c = new CDbCriteria();
        $c->order = 'startperiod desc';
        $c->compare('cust_id',$this->id);
        $c->compare('startperiod','<='.$batch_id);
        $contract = Contract::model()->find($c);
        return $contract;
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('activecontract_id, bank_id', 'numerical', 'integerOnly'=>true),
			array('name, address, address2', 'length', 'max'=>128),
			array('trunkcode, phone, invoicecode, displayformat', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, address, address2, trunkcode, phone, activecontract_id, invoicecode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activecontract' => array(self::BELONGS_TO, 'Contract', 'activecontract_id'),
			'invoices' => array(self::HAS_MANY, 'Invoice', 'cust_id'),
			'voiceCdrs' => array(self::HAS_MANY, 'VoiceCdr', 'cust_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'address' => 'Address',
			'address2' => ' ',
			'trunkcode' => 'Trunkcode',
			'phone' => 'Customer ID',
			'activecontract_id' => 'Activecontract',
			'invoicecode' => 'Invoicecode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('trunkcode',$this->trunkcode,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('activecontract_id',$this->activecontract_id);
		$criteria->compare('invoicecode',$this->invoicecode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
