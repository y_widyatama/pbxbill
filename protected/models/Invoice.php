<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property integer $id
 * @property string $invoiceno
 * @property string $invoicedate
 * @property string $duedate
 * @property integer $period
 * @property integer $cust_id
 * @property string $invoiceamount
 * @property string $displayformat
 *
 * The followings are the available model relations:
 * @property Customer $cust
 * @property Contract $activecontract
 */
class Invoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('period, cust_id', 'numerical', 'integerOnly'=>true),
			array('invoiceno', 'length', 'max'=>45),
			//array('invoiceamount', 'length', 'max'=>9),
			array('invoicedate, duedate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoiceno, invoicedate, duedate, period, cust_id, invoiceamount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'Customer', 'cust_id'),
			'activecontract' => array(self::BELONGS_TO, 'Contract', 'activecontract_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoiceno' => 'Invoice No',
			'invoicedate' => 'Invoice Date',
			'invoicedateFormatted' => 'Invoice Date',
			'duedate' => 'Due date',
			'duedateFormatted' => 'Due date',
			'period' => 'Period',
			'periodFormatted' => 'Billing Period',
		        'durationSecs' => 'Duration (s)',	
			'cust_id' => 'Cust',
			'invoiceamount' => 'Invoice amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoiceno',$this->invoiceno,true);
		$criteria->compare('invoicedate',$this->invoicedate,true);
		$criteria->compare('duedate',$this->duedate,true);
		$criteria->compare('period',$this->period);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('invoiceamount',$this->invoiceamount,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public $voicesummary = array();
	public function consume(VoiceCdr $cdr)
	{
		$this->invoiceamount = $this->invoiceamount + $cdr->ratedcharge;
        $key = Tvoice::createKey($cdr);
        if (!array_key_exists($key,$this->voicesummary))
            $this->voicesummary[$key] = Tvoice::createEmptyFromCdr($cdr);
        $voiceRecord = $this->voicesummary[$key];
        $voiceRecord->consume($cdr);
	}
    public function saveVoiceSummaries()
    {
        foreach ($this->voicesummary as $voiceRecord)
            $voiceRecord->save();
    }
	public static function createNew($batch_id,$customer)
	{
		$existing = Invoice::model()->findByAttributes (array('period'=>$batch_id,'cust_id'=>$customer->id));
		if ($existing) 
			$n=$existing; 
		else
			$n = new Invoice();
		$n->period = $batch_id;
		$n->cust_id = $customer->id;
		$mon = $batch_id % 100;
		if ($mon<10) $monstr = '0'.$mon; else $monstr = $mon;
		$yr = intval(floor($batch_id /100)) % 100;
		if ($yr<10) $yrstr = '0'.$yr; else $yrstr = $yr;
		$n->invoiceno = 'TELINHK'.$yrstr.$monstr.'-'.$customer->invoicecode.'-08';
		$n->invoicedate = date('Y-m-d');
        $n->duedate = date('Y-m-d', strtotime("+30 days"));		
		$n->invoiceamount = 0;
		$n->displayformat = $customer->displayformat;
        $contract = $customer->getContract($batch_id);
		$n->activecontract_id = $contract->id;
		return $n;
	}
	public function getInvoicedateFormatted()
    {
		$d = explode('-',$this->invoicedate);
        $yr = intval($d[0]);
		$mon =  intval( $d[1]);
		$day =  intval($d[2]);
        $t = mktime(0,0,0,$mon,$day,$yr);
        return date('d-F-Y',$t);
    }
	public function getDuedateFormatted()
    {
        if (empty($this->duedate)) return null;
		$d = explode('-',$this->duedate);
        $yr = intval($d[0]);
		$mon =  intval( $d[1]);
		$day =  intval($d[2]);
        $t = mktime(0,0,0,$mon,$day,$yr);
        return date('d-F-Y',$t);
    }

	public function getPeriodFormatted()
    {
        $yr = floor($this->period/ 100.0);
        $mon = $this->period % 100;
        $t = mktime(0,0,0,$mon,1,$yr);
        return date('F Y',$t);
    }
	public function isFileExists()
	{
	    if (file_exists('output/inv'.$this->id.'.pdf')) return true;
		return false;
	}
	public function fileUrl()
	{
		if ($this->isFileExists()) return 'output/inv'.$this->id.'.pdf';
	    return '';
	}
	public function getDurationColumnName()
	{
		if ($this->displayformat == 's') return 'durationSecs';
		return 'durationR';
	}
	public function getDurationTitle()
	{
		$labels = $this->attributeLabels();
		if (!isset ($labels [ $this->getDurationColumnName()]))
			return null;
		return $labels [ $this->getDurationColumnName()] ;
	}
	public function getTotalDurationColumn()
	{
		if ($this->displayformat == 's')
			return 'totaldurationSecs';
		else return 'totaldurationMinutes';
	}
}
