<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PeriodForm extends CFormModel
{
	public $batch_id;
    public $cust_id;
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('batch_id, cust_id', 'numerical'),
		);
	}
	public function getDropDown($withAll = false)
	{
		$r = array();
//		$z = array('201301','201302','201303','201304','201305','201306','201307','201308');
		$z = VoiceCdr::distinctBatchIds();
		if ($withAll) $r['0']='ALL';
		foreach ($z as $zrow)
		  $r[$zrow] = $zrow;
		return $r;
	}
    public function getDropDownCust()
    {
        $allCusts = Customer::model()->findAll();
        $r = array();
        $r['0'] = 'ALL';
        foreach ($allCusts as $c)
            $r[$c->id] = $c->name;
        return $r;
    }
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'batch_id'=>'Billing Period',
		);
	}

}
