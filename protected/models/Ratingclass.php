<?php

/**
 * This is the model class for table "ratingclass".
 *
 * The followings are the available columns in table 'ratingclass':
 * @property integer $id
 * @property integer $tariffschema_id
 * @property integer $roundmode
 * @property string $destinationnet
 * @property string $code
 * @property string $rateusd
 * @property string $wholesalehkd
 * @property string $retailhkd
 */
class Ratingclass extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ratingclass the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ratingclass';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('destinationnet', 'length', 'max'=>100),
			array('code', 'length', 'max'=>16),
			array('rateusd, wholesalehkd, retailhkd', 'length', 'max'=>10),
            array('tariffschema_id, roundmode', 'numerical', 'integerOnly'=>true),

            // The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, destinationnet, code, rateusd, wholesalehkd, retailhkd, tariffschema_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'destinationnet' => 'Destination Network',
			'code' => 'Code',
			'rateusd' => 'Rateusd',
			'wholesalehkd' => 'Wholesalehkd',
			'retailhkd' => 'Rate ($/min)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('destinationnet',$this->destinationnet,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('rateusd',$this->rateusd,true);
		$criteria->compare('wholesalehkd',$this->wholesalehkd,true);
		$criteria->compare('retailhkd',$this->retailhkd,true);
        $criteria->compare('tariffschema_id',$this->tariffschema_id,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}