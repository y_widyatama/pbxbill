<?php

/**
 * This is the model class for table "tariff_schema".
 *
 * The followings are the available columns in table 'tariff_schema':
 * @property integer $id
 * @property string $name
 * @property string $dateref
 * @property string $info
 * @property string $currency
 *
 * The followings are the available model relations:
 * @property Contract[] $contracts
 * @property Ratingclass[] $ratingclasses
 */
class TariffSchema extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TariffSchema the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tariff_schema';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>120),
			array('info', 'length', 'max'=>1000),
			array('dateref', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('currency', 'length', 'max'=>50),
			array('id, name, dateref, currency, info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contracts' => array(self::HAS_MANY, 'Contract', 'schema_id'),
			'ratingclasses' => array(self::HAS_MANY, 'Ratingclass', 'tariffschema_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'dateref' => 'Dateref',
			'curreny' => 'Currency',
			'info' => 'Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('dateref',$this->dateref,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}