<?php

/**
 * This is the model class for table "tvoice".
 *
 * The followings are the available columns in table 'tvoice':
 * @property integer $id
 * @property string $networkname
 * @property string $totalcharge
 * @property integer $batch_id
 * @property string $originatingtrunkgroupclli
 * @property string $originatingnumber
 * @property integer $ratingclass_id
 * @property integer $totalduration
 * @property integer $cust_id
 *
 * The followings are the available model relations:
 * @property Customer $cust
 */
class Tvoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tvoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tvoice';
	}

    public function getGroupName()
    {
        return "Services for ".$this->originatingnumber;
    }
    public function setGroupName($z)
    {
    //    $this->hasAttribute('groupName');
    }
    public function hasAttribute($n)
    {
        if ($n == 'groupName') return true;
        return parent::hasAttribute($n);
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id, ratingclass_id, totalduration, cust_id', 'numerical', 'integerOnly'=>true),
			array('networkname, originatingtrunkgroupclli, originatingnumber', 'length', 'max'=>45),
			array('totalcharge', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, networkname, totalcharge, batch_id, originatingtrunkgroupclli, originatingnumber, ratingclass_id, totalduration, cust_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'Customer', 'cust_id'),
			'ratingclass' => array(self::BELONGS_TO, 'Ratingclass', 'ratingclass_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'networkname' => 'Destination Network',
			'totalcharge' => 'Totalcharge',
			'batch_id' => 'Batch',
			'originatingtrunkgroupclli' => 'Originatingtrunkgroupclli',
			'ratingclass_id' => 'Ratingclass',
			'totalduration' => 'Totalduration',
			'cust_id' => 'Cust',
		);
	}
	public function getTotaldurationMinutes()
	{
		if ($this->ratingclass->retailhkd == 0)
			return ( $this->totalduration * 0.01 )/60.0;
	    return round($this->totalcharge / $this->ratingclass->retailhkd);
	}
	public function getTotaldurationSecs()
	{
		if ($this->ratingclass->retailhkd == 0)
			return $this->totalduration * 0.01;
	    return round($this->totalcharge * 60.0/ $this->ratingclass->retailhkd);
	}
	public function totaldurationIn($displayformat)
	{
		if ($displayformat == 's')
			return $this->getTotaldurationSecs();
		return $this->getTotaldurationMinutes();
	}
    public function getTotaldurationFormatted()
    {
        $secs = ceil($this->totalduration * 0.01);

        return MyFormatter::minsecs($secs);
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($noPage=false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('networkname',$this->networkname,true);
		$criteria->compare('totalcharge',$this->totalcharge,true);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('originatingtrunkgroupclli',$this->originatingtrunkgroupclli,true);
        $criteria->compare('originatingnumber',$this->originatingnumber,true);
		$criteria->compare('ratingclass_id',$this->ratingclass_id);
		$criteria->compare('totalduration',$this->totalduration);
		$criteria->compare('cust_id',$this->cust_id);
	$criteria->order = "originatingnumber,ratingclass_id";
        if ($noPage)
        return new CActiveDataProvider($this, array('criteria'=>$criteria, 'pagination'=>array('pageSize'=>100000)) );
        else
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
	}
	
	public function searchRev() // group by networkname instead of originatingnumber
	{
		$criteria=new CDbCriteria;
		$criteria->select = 'networkname,sum(totalcharge) totalcharge,sum(totalduration) totalduration,ratingclass_id';
		$criteria->group = "networkname,ratingclass_id";
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('cust_id',$this->cust_id);
        return new CActiveDataProvider($this, array('criteria'=>$criteria, 'pagination'=>array('pageSize'=>100000)) );
	}

    public static function createKey(VoiceCdr $cdr)
    {
        return $cdr->ratingclass_id.':'.$cdr->originating_number;
    }
    public static function createEmptyFromCdr(VoiceCdr $cdr)
    {
        $n = new Tvoice();
        $n->originatingtrunkgroupclli = $cdr->origtrunkgroupclli;
        $n->networkname = $cdr->ratingclass->destinationnet;
        $n->ratingclass_id = $cdr->ratingclass_id;
        $n->originatingnumber = $cdr->originating_number;
        $n->batch_id = $cdr->batch_id;
        $n->cust_id = $cdr->cust_id;
        return $n;
    }
    public function consume(VoiceCdr $cdr)
    {
        $this->totalcharge = $this->totalcharge + $cdr->ratedcharge;
        $this->totalduration = $this->totalduration  + 100 * $cdr->roundedDuration();
    }
}
