<?php

/**
 * This is the model class for table "voice_cdr".
 *
 * The followings are the available columns in table 'voice_cdr':
 * @property integer $id
 * @property integer $batch_id
 * @property integer $cust_id
 * @property string $originating_number
 * @property string $destination_number
 * @property string $origtrunkgroupclli
 * @property string $origtrunkgroup
 * @property integer $duration
 * @property string $rate
 * @property integer $ratingclass_id
 * @property string $ratedcharge
 * @property string $start
 * @property string $stop
 *
 * The followings are the available model relations:
 * @property Customer $cust
 * @property Ratingclass $ratingclass
 */
class VoiceCdr extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VoiceCdr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'voice_cdr';
	}

	public static function distinctBatchIds()
	{
		$c = new CDbCriteria();
		$c->select = 'batch_id';
		$c->distinct = true;
		$batches = VoiceCdr::model()->findAll($c);
		$r = array();
		foreach ($batches as $v)
		{
		   $r[] = $v->batch_id;
		}
		return $r;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id, cust_id, duration, ratingclass_id', 'numerical', 'integerOnly'=>true),
			array('originating_number, destination_number, origtrunkgroupclli, origtrunkgroup', 'length', 'max'=>45),
			array('rate, ratedcharge', 'numerical'),
			array('start, stop', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, batch_id, cust_id, originating_number, destination_number, origtrunkgroupclli, origtrunkgroup, duration, rate, ratingclass_id, ratedcharge, start, stop', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'Customer', 'cust_id'),
			'ratingclass' => array(self::BELONGS_TO, 'Ratingclass', 'ratingclass_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'batch_id' => 'Batch',
			'cust_id' => 'Cust',
			'originating_number' => 'Originating Number',
			'destination_number' => 'Destination Number',
			'origtrunkgroupclli' => 'Origtrunkgroupclli',
			'origtrunkgroup' => 'Origtrunkgroup',
			'duration' => 'Duration',
			'durationR' => 'Duration (min)',
			'durationSecs' => 'Duration (s)',
			'rate' => 'Rate (HK$/min)',
			'ratingclass_id' => 'Ratingclass',
			'ratedcharge' => 'Charge (HK$)',
			'start' => 'Start',
			'stop' => 'Stop',
		);
	}
	public function getDurationR()
	{
		/*$secs = ceil($this->duration * 0.01);
		$mins = '';
		if ($secs>60) {
		  $mins = floor($secs/60);
		  $mincnt = $mins;
		  if ($mins<10) $mins = '0'.intval($mins); else $mins = intval($mins);
		  $mins .= ':';
		} else $mincnt = 0;
		$remsecs = $secs - 60*$mincnt;
		if ($remsecs < 10) $remsecs = '0'.$remsecs;
		return $mins . $remsecs;*/
		$mins = ceil($this->duration * 0.01/60);
		return $mins;
	}
	public function getDurationSecs()
	{
		/*$secs = ceil($this->duration * 0.01);
		$mins = '';
		if ($secs>60) {
		  $mins = floor($secs/60);
		  $mincnt = $mins;
		  if ($mins<10) $mins = '0'.intval($mins); else $mins = intval($mins);
		  $mins .= ':';
		} else $mincnt = 0;
		$remsecs = $secs - 60*$mincnt;
		if ($remsecs < 10) $remsecs = '0'.$remsecs;
		return $mins . $remsecs;*/
		$secs = ceil($this->duration * 0.01);
		return $secs;
	}


	public function doRating()
	{
	   $digits = 10;
        if (empty($this->cust_id))
        {
            $trunkgroup = $this->origtrunkgroupclli;
        	$cust = Customer::model()->findByAttributes(array('trunkcode'=>$trunkgroup));
        	if ($cust) $this->cust_id= $cust->id;
        }
        $custSchemaId = Customer::getSchemaId($this->cust_id,$this->batch_id);
	   for ($digits=10; $digits>=1; $digits--)
	   {
	      $codedigits = substr($this->destination_number,0,$digits);
	      $r = Ratingclass::model()->findByAttributes(array('code'=>$codedigits,
              'tariffschema_id'=>$custSchemaId));
		  if (!empty($r))
		  {
		     
		     break;
		  }
	   }
	   if (!empty($r))
	   {
	      $this->ratingclass_id = $r->id;
		  $this->rate = $r->retailhkd;
		  $noCharge = false;
		  if (empty($r->roundmode) || ($r->roundmode >10))
		  	 $noCharge = ($this->duration < 200);
		  else
		  	 $noCharge = $this->duration < 5;
		  if ($noCharge) $this->ratedcharge =0;
		  else
		  {
			  if (empty($r->roundmode))
			  {
				  //$this->ratedduration = ceil($this->duration / 6000);
		  	      $this->ratedcharge = $this->rate * ceil($this->duration / 6000);
			  } else {
				  $rm = $r->roundmode;
				  $this->ratedcharge = $this->rate * (1.0/60) * $rm * ceil($this->duration / (100 * $rm));
				  //$this->ratedduration = (1.0/60) * $rm * ceil($this->duration / (100 * $rm)) ;
			  }
		  }
	   }
	}
	public function roundedDuration() {
		$roundmode = $this->ratingclass->roundmode;
		if (empty($roundmode) || ($roundmode >10))
		  	 $noCharge = ($this->duration < 200);
		  else
			   $noCharge = $this->duration < 5;
		IF ($noCharge) return 0;
		if (empty($roundmode))
			return ceil($this->duration / 6000)*60;
		$result=  $roundmode * ceil($this->duration / (100 * $roundmode));
		return $result;
	}
    public static function batchMirror($batch_id, $page=-1)
    {
       $mirrorFrom = array('Apple'=>'GIJKFC','AppleHuawei'=>'GECFSHKSJK1');
       $mirrorTo = array('Apple'=>'ZGIJKFC','AppleHuawei' => 'ZGIJKFC');
       $res = "";
       foreach ($mirrorFrom as $k => $currentFrom)
       {
           $currentTo = $mirrorTo[$k];
           $res .= "<br>Dari $currentFrom ke $currentTo <br>";
           $c = new CDbCriteria();
           if ($page >= 0)
           { 
              $c->offset = $page * 10000;
              $c->limit = 10000;
           }
           $c->compare('batch_id',intval($batch_id));
           $c->compare('origtrunkgroupclli',$currentFrom);
           $cdrlist = VoiceCdr::model()->findAll($c);
           $ok=0;$fail=0; $skip=0;
           foreach ($cdrlist as $cdr)
           {
               if ($cdr->sys_gen>0) { $skip++; continue; }
               $cdrM = new VoiceCdr();
               $cdrM->batch_id = $cdr->batch_id;
               $cdrM->originating_number = $cdr->originating_number;
               $cdrM->destination_number = $cdr->destination_number;
               $cdrM->origtrunkgroupclli = $currentTo;
               $cdrM->origtrunkgroup = $cdr->origtrunkgroup;
               $cdrM->duration= $cdr->duration;
               $cdrM->start= $cdr->start;
               $cdrM->stop= $cdr->stop;
							 $cdrM->cdr_id= $cdr->cdr_id;
							 $cdrM->cdr2_id= $cdr->cdr2_id;
               $cdrM->sys_gen = 101;
               if ($cdrM->save())
               {
                   $cdr->sys_gen = 200;
                   if (!$cdr->save()) {
                $fail++;
                $e1 = $cdr->getErrors();
                $attrs = $cdr->attributes;
                $e = var_export($e1,true);
                $a1 = var_export($attrs,true);
                $res .= $e . ":". $a1."<br>";
                   } else
                  $ok++;
               } else {
                $fail++;
                $e1 = $cdrM->getErrors();
                $e = var_export($e1,true);
                $res .= $e . "<br>";
               }
               
           }
           $res .= " ok = ".$ok." skip = $skip fail = $fail <br>";

       }
       return $res;
    }
    
	public static function batchRating($batch_id, $page=-1)
	{
	   $c = new CDbCriteria();
	   if ($page >= 0)
	   { 
	      $c->offset = $page * 10000;
		  $c->limit = 10000;
	   }
	   $c->compare('batch_id',intval($batch_id));
	   $cdrlist = VoiceCdr::model()->findAll($c);
	   $cnt = 0;
	   foreach ($cdrlist as $cdr)
	   {
		  if ($cdr->ratingclass_id) continue;
		  $cdr->doRating();
		  $cdr->save();
		  $cnt++;
	   }
	   $cnt++;
	}
	
	public static function batchInvoice($batch_id,$cust_id=0)
	{
	   $customers = array();
	   $invoices = array();
        /**
         * @var Invoice[] $invoices
         */
       //$this->dbConnection->beginTransaction();
       if ($cust_id>0)
         Tvoice::model()->deleteAllByAttributes(array('batch_id'=>$batch_id,'cust_id'=>$cust_id));
       else
	     Tvoice::model()->deleteAllByAttributes(array('batch_id'=>$batch_id));
       if ($cust_id>0)
         $cdrlist = VoiceCdr::model()->findAllByAttributes(array('batch_id'=>$batch_id,'cust_id'=>$cust_id));
       else 
	     $cdrlist = VoiceCdr::model()->findAllByAttributes(array('batch_id'=>$batch_id));
	   foreach ($cdrlist as $cdr)
	   {
		  if (!$cdr->ratingclass_id) continue;
		  if (!$cdr->cust_id)  continue;
		  if (!array_key_exists($cdr->cust_id,$customers))
		  {
		    $customers[$cdr->cust_id] = $cdr->cust;
			$invoices[$cdr->cust_id] = Invoice::createNew($batch_id,$cdr->cust);
		  }

		  $invo = $invoices[$cdr->cust_id];
		  $invo->consume($cdr);
	   }
	   foreach ($invoices as $invo)
       {
	    $invo->save();
        $invo->saveVoiceSummaries();
       }
	}

	public static function clearRating($batch_id)
	{
	   $cdrlist = VoiceCdr::model()->findAllByAttributes(array('batch_id'=>$batch_id));
	   foreach ($cdrlist as $cdr)
	   {
		  if (!$cdr->ratingclass_id) continue;
		  $cdr->ratingclass_id = null;
		  $cdr->save();
	   }
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($noPage=false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('originating_number',$this->originating_number,true);
		$criteria->compare('destination_number',$this->destination_number,true);
		$criteria->compare('origtrunkgroupclli',$this->origtrunkgroupclli,true);
		$criteria->compare('origtrunkgroup',$this->origtrunkgroup,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('rate',$this->rate,true);
		$criteria->compare('ratingclass_id',$this->ratingclass_id);
		//$criteria->compare('ratedcharge',$this->ratedcharge,true);
		
		// change filter from ratedcharge to duration
		//$criteria->compare('ratedcharge','>0');
		$criteria->compare('duration','>=200');

		$criteria->compare('start',$this->start,true);
		$criteria->compare('stop',$this->stop,true);
        $criteria->order = 'start';
		if ($noPage)
		return new CActiveDataProvider($this, array('criteria'=>$criteria, 'pagination'=>array('pageSize'=>100000)) );
		else
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

	}
	public function searchDistinctDests()
	{
		$criteria = new CDbCriteria();
		$criteria->distinct = true;
		$criteria->select = array('batch_id','cust_id','destination_number');
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('cust_id',$this->cust_id);
		$result = $this->findAll($criteria);
		return $result;
	}
}
