<?php

class CdrStatusTest extends WebTestCase
{
	public $fixtures=array(
		'cdrStatuses'=>'CdrStatus',
	);

	public function testShow()
	{
		$this->open('?r=cdrStatus/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=cdrStatus/create');
	}

	public function testUpdate()
	{
		$this->open('?r=cdrStatus/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=cdrStatus/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=cdrStatus/index');
	}

	public function testAdmin()
	{
		$this->open('?r=cdrStatus/admin');
	}
}
