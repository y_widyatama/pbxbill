<?php
/* @var $this BankinfoController */
/* @var $model Bankinfo */

$this->breadcrumbs=array(
	'Bankinfos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bankinfo', 'url'=>array('index')),
	array('label'=>'Manage Bankinfo', 'url'=>array('admin')),
);
?>

<h1>Create Bankinfo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>