<?php
/* @var $this BankinfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bankinfos',
);

$this->menu=array(
	array('label'=>'Create Bankinfo', 'url'=>array('create')),
	array('label'=>'Manage Bankinfo', 'url'=>array('admin')),
);
?>

<h1>Bankinfos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
