<?php
/* @var $this BankinfoController */
/* @var $model Bankinfo */

$this->breadcrumbs=array(
	'Bankinfos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bankinfo', 'url'=>array('index')),
	array('label'=>'Create Bankinfo', 'url'=>array('create')),
	array('label'=>'View Bankinfo', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage Bankinfo', 'url'=>array('admin')),
);
?>

<h1>Update Bankinfo <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>