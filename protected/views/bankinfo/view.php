<?php
/* @var $this BankinfoController */
/* @var $model Bankinfo */

$this->breadcrumbs=array(
	'Bankinfos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Bankinfo', 'url'=>array('index')),
	array('label'=>'Create Bankinfo', 'url'=>array('create')),
	array('label'=>'Update Bankinfo', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete Bankinfo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bankinfo', 'url'=>array('admin')),
);
?>

<h1>View Bankinfo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'companyname',
		'bankdetail',
		'account',
		'address',
		'swiftcode',
	),
)); ?>
