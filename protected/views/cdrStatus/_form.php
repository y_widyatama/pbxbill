<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cdr-status-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_processed'); ?>
		<?php echo $form->textField($model,'is_processed'); ?>
		<?php echo $form->error($model,'is_processed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'process_time'); ?>
		<?php echo $form->textField($model,'process_time'); ?>
		<?php echo $form->error($model,'process_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'process_cnt'); ?>
		<?php echo $form->textField($model,'process_cnt'); ?>
		<?php echo $form->error($model,'process_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->