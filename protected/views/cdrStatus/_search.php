<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_processed'); ?>
		<?php echo $form->textField($model,'is_processed'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'process_time'); ?>
		<?php echo $form->textField($model,'process_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'process_cnt'); ?>
		<?php echo $form->textField($model,'process_cnt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->