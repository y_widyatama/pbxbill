<?php
/* @var $this CdrStatusController */
/* @var $data CdrStatus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_processed')); ?>:</b>
	<?php echo CHtml::encode($data->is_processed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('process_time')); ?>:</b>
	<?php echo CHtml::encode($data->process_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('process_cnt')); ?>:</b>
	<?php echo CHtml::encode($data->process_cnt); ?>
	<br />
    <b><?php echo CHtml::encode($data->getAttributeLabel('archive_size')); ?>:</b>
	<?php echo CHtml::encode($data->archiveSize); ?>
	<br />


</div>
