<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */

$this->breadcrumbs=array(
	'Cdr Statuses'=>array('index'),
	'Batch Archive',
);

$this->menu=array(
	array('label'=>'List CdrStatus', 'url'=>array('index')),
    array('label'=>'Manage CdrStatus', 'url'=>array('admin')),
    array('label'=>'Batch Archive CdrStatus', 'url'=>array('batchArchive')),
);
?>

<h1>Batch Archive CdrStatus</h1>

<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cdr-status-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
		<?php echo CHtml::textField('fromId',$firstProcessed->id); ?>
	</div>

	<div class="row">
        <?php echo CHtml::textField('toId',$lastProcessed->id); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Archive'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->