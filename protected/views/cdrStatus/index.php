<?php
/* @var $this CdrStatusController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cdr Statuses',
);

$this->menu=array(
	array('label'=>'Create CdrStatus', 'url'=>array('create')),
	array('label'=>'Manage CdrStatus', 'url'=>array('admin')),
);
?>

<h1>Cdr Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
