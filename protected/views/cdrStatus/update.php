<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */

$this->breadcrumbs=array(
	'Cdr Statuses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CdrStatus', 'url'=>array('index')),
	array('label'=>'Create CdrStatus', 'url'=>array('create')),
	array('label'=>'View CdrStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CdrStatus', 'url'=>array('admin')),
);
?>

<h1>Update CdrStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>