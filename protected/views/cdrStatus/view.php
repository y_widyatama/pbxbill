<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */

$this->breadcrumbs=array(
	'Cdr Statuses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CdrStatus', 'url'=>array('index')),
	array('label'=>'Create CdrStatus', 'url'=>array('create')),
	array('label'=>'Update CdrStatus', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete CdrStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Archive Cdr File', 'url'=>array('archive', 'id'=>$model->id)),
	array('label'=>'Manage CdrStatus', 'url'=>array('admin')),
);
?>

<h1>View CdrStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'is_processed',
		'process_time',
		'process_cnt',
        'archiveSize',
        'archiveName',
	),
)); ?>
