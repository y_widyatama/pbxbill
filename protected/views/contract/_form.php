<?php
/* @var $this ContractController */
/* @var $model Contract */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contract-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'contractno'); ?>
		<?php echo $form->textField($model,'contractno',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'contractno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contractdate'); ?>
		<?php
        // echo $form->textField($model,'contractdate');
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,
            'attribute'=>'contractdate',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'yy-mm-dd',
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));
        ?>
		<?php echo $form->error($model,'contractdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contractend'); ?>
		<?php
        //echo $form->textField($model,'contractend');
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,
            'attribute'=>'contractend',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat'=>'yy-mm-dd',
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));
        ?>
		<?php echo $form->error($model,'contractend'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
		<?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'schema_id'); ?>
		<?php echo $form->textField($model,'schema_id'); ?>
		<?php echo $form->error($model,'schema_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->