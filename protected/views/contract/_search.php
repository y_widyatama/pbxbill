<?php
/* @var $this ContractController */
/* @var $model Contract */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contractno'); ?>
		<?php echo $form->textField($model,'contractno',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contractdate'); ?>
		<?php echo $form->textField($model,'contractdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contractend'); ?>
		<?php echo $form->textField($model,'contractend'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'schema_id'); ?>
		<?php echo $form->textField($model,'schema_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->