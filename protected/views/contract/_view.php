<?php
/* @var $this ContractController */
/* @var $data Contract */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contractno')); ?>:</b>
	<?php echo CHtml::encode($data->contractno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_id')); ?>:</b>
	<?php echo CHtml::encode($data->cust_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contractdate')); ?>:</b>
	<?php echo CHtml::encode($data->contractdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contractend')); ?>:</b>
	<?php echo CHtml::encode($data->contractend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startperiod')); ?>:</b>
	<?php echo CHtml::encode($data->startperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('schema_id')); ?>:</b>
	<?php echo CHtml::encode($data->schema_id); ?>
	<br />


</div>