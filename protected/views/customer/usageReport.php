<?php
/* @var $this CustomerController */
/* @var $model Customer */
$period = $periodM->batch_id;
$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->name,
);
$invoice = Invoice::createNew($period, $model);
$currency = CHtml::value($invoice,'activecontract.tariffSchema.currency','empty');

$this->menu=array(
	array('label'=>'View Customer', 'url'=>array('view',
	   'id'=>$model->id)),

	array('label'=>'Manage Customer', 'url'=>array('admin')),
);
?>

<h1>Usage for Customer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address',
		'address2',
		'trunkcode',
		'phone',
		'activecontract_id',
		'invoicecode',
	),
)); ?>

<?php
$actionUrl = array('/customer/reportUsage','cust_id'=>$model->id);
$method = 'post';
$autosubmit = false;
$d = compact('actionUrl','method','autosubmit');
$d['model'] = $periodM;
$this->renderPartial('/periodform',$d);
?>

<h2>Usage</h2>
<?php

  $modelvoice = new VoiceCdr();
  $modelvoice->cust_id = $cust_id; 
  $modelvoice->batch_id = $period;
$mf = new MyFormatter();
$provider = $modelvoice->search(true);
$data = $provider->getData();
$duration = 0; $charge = 0;
foreach ($data as $row)
{
   $duration += $row->durationR;
   $charge += $row->ratedcharge;
}

 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$modelvoice->search(true),
	//'filter'=>$modelvoice,
    'formatter' => $mf->setDecimals(2),
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Voice Call Records',
	'columns'=>array(
		'start','stop','destination_number','ratingclass.destinationnet',
		array
		('name'=>'durationR','cssClassExpression'=>'"rightalign"',
		  'footer'=>$duration,
          'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;')
		),
		array('name'=>'rate','type'=>'number','cssClassExpression'=>'"rightalign"','header' =>'Rate ('.$currency.'/min)',
		),
		array('name'=>'ratedcharge','type'=>'number','cssClassExpression'=>'"rightalign"', 'header'=>'Charge ('.$currency.')',
			'footer'=>$charge,
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;')

		),

	),
)); 


?>
