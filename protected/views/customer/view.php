<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->name,
);

$currentPeriod = intval(date('Ym'));
$this->menu=array(
	array('label'=>'Update Customer', 'url'=>array('update',
	   'id'=>$model->id)),
	array('label'=>'Delete Customer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Create Contract', 'url'=>array('/contract/create',
    	   'cust_id'=>$model->id)),
    array('label'=>'Usage Report', 'url'=>array('reportUsage',
    	   'cust_id'=>$model->id, 'period'=>$currentPeriod)),

	array('label'=>'Manage Customer', 'url'=>array('admin')),
);
?>

<h1>View Customer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address',
		'address2',
		'trunkcode',
		'phone',
		'activecontract_id',
		'invoicecode',
		'displayformat',
	),
)); ?>

<h2>Contracts</h2>

<?php
    $cmodel = new Contract();
    $cmodel->cust_id = $model->id;
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contract-grid',
	'dataProvider'=>$cmodel->search(),
	//'filter'=>$cmodel,
	'columns'=>array(
		'id',
		'contractno',
		'cust_id',
		'contractdate',
		'contractend',
		'startperiod',

		'schema_id',

		array(
			'class'=>'CButtonColumn',
            'viewButtonUrl'=>'array("/contract/view","id"=>$data->id)',
            'updateButtonUrl'=>'array("/contract/update","id"=>$data->id)',
            'deleteButtonUrl'=>'array("/contract/delete","id"=>$data->id)',
		),
	),
)); ?>
