<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Update Invoice', 'url'=>array('update',
	   'id'=>$model->id)),
	array('label'=>'Delete Invoice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invoice', 'url'=>array('admin')),
    array('label'=>'Print version', 'url'=>array('preview','id'=>$model->id)),
	array('label'=>'Print version (Incoming)', 'url'=>array('viewRev','id'=>$model->id)),
    array('label'=>'PDF version', 'url'=>array('viewPdf','id'=>$model->id)),
	array('label'=>'PDF version (Incoming)', 'url'=>array('viewPdfRev','id'=>$model->id)),
    array('label'=>'Download CSV', 'url'=>array('downloadCsv','id'=>$model->id, 'format'=>2)),

	
);
?>

<h1>View Invoice #<?php echo $model->id; ?></h1>

<style type="text/css">
.rightalign {text-align: right; }
body {background-color: white; }
.grid-view table.items th { color: white; background: #6a6a7a no-repeat; }
</style>

<h2 style="text-align: center;">INVOICE</h2>
<b>Bill to:</b>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model->cust,
	'attributes'=>array(
		'name',
		'address',
		'address2',
	),
)); 

$currency = CHtml::value($model,'activecontract.tariffSchema.currency','empty');

?>
<br>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'invoiceno',
		'invoicedateFormatted',
		'activecontract.contractno',
		'activecontract.contractdate',
		'duedateFormatted',
		'periodFormatted',
        'cust.phone',
		'invoiceamount',
	),
)); ?>


<?php
  $modeltv = new Tvoice();
  $modeltv->batch_id = $model->period;
  $modeltv->cust_id = $model->cust_id;
  $tvprovider = $modeltv->search(true);
  $voiceSummaryRows = $tvprovider->getData(true);
    $totalcharge = 0.0;
/**
 * @var Tvoice $tv
 */
  foreach ($voiceSummaryRows as $tv)
  {
        $totalcharge += $tv->totalcharge;
  }
  $modelvoice = new VoiceCdr();
  $modelvoice->cust_id = $model->cust->id; 
  $modelvoice->batch_id = $model->period;
$mf = new MyFormatter();

  ?>
<?php $this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'voicesummary-grid',
	'dataProvider'=>$tvprovider,
    'extraRowColumns' => array('originatingnumber'),
    'extraRowPos' => 'above',
    'extraRowExpression' => '"<b>Service for ".$data->originatingnumber."</b>"',
    'formatter'=> $mf->setDecimals(2),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Invoice items',
	'columns'=>array(
        'ratingclass.code:text:Destination prefix',
        array('name'=>'networkname','footer'=>'Total'),
        array('name'=>'totalcharge','header'=>'Charge ('.$currency.')',
            'type'=>'number',
            'footer'=>number_format($totalcharge,2),'cssClassExpression'=>'"rightalign"',
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;'),

        ),

        array('name'=>'totalduration','header'=>'Duration'),
	),
)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$modelvoice->search(true),
	//'filter'=>$modelvoice,
    'formatter' => $mf->setDecimals(2),
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Voice Call Records',
	'columns'=>array(
		'start','stop','destination_number','ratingclass.destinationnet',array('name'=>$model->getDurationColumnName(),'cssClassExpression'=>'"rightalign"'),
		'rate:number:Rate ('.$currency.')/min',
        array('name'=>'ratedcharge','type'=>'number','cssClassExpression'=>'"rightalign"','header'=>'Charge ('.$currency.')'),

	),
)); ?>
