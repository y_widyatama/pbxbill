<?php
/* @var $this InvoiceController */
/* @var $model Invoice */
$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Update Invoice', 'url'=>array('update',
	   'id'=>$model->id)),
	array('label'=>'Delete Invoice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invoice', 'url'=>array('admin')),
    array('label'=>'Print version', 'url'=>array('preview','id'=>$model->id)),
    array('label'=>'PDF version', 'url'=>array('viewPdf','id'=>$model->id)),

);


?>


<style type="text/css">
.rightalign {text-align: right; }
body {background-color: white; }
.grid-view table.items th { color: white; background: #6a6a7a no-repeat; }
</style>

<b>Bill to:</b>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model->cust,
	'attributes'=>array(
		'name:ntext',
		'address',
		'address2',
	),
));
$currency = CHtml::value($model,'activecontract.tariffSchema.currency','empty');

?>
<br>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'formatter'=>new MyFormatter($currency),
	'attributes'=>array(
		'invoiceno',
		'invoicedateFormatted',
		'activecontract.contractno',
		'activecontract.contractdate',
		'duedateFormatted',
		'periodFormatted',
        
        array(
            'name' => 'invoiceamount',
            'type' => 'currency',
            )
	),
)); ?>


<?php
  $modeltv = new Tvoice();
  $modeltv->batch_id = $model->period;
  $modeltv->cust_id = $model->cust_id;
  $tvprovider = $modeltv->searchRev();
  $voiceSummaryRows = $tvprovider->getData(true);
    $totalcharge = 0.0;
/**
 * @var Tvoice $tv
 */
$totalDuration = 0;
  foreach ($voiceSummaryRows as $tv)
  {
        $totalcharge += $tv->totalcharge;
      $totalDuration += $tv->totaldurationIn($model->displayformat);
  }
  $modelvoice = new VoiceCdr();
  $modelvoice->cust_id = $model->cust->id; 
  $modelvoice->batch_id = $model->period;
  $distinctDestinations = $modelvoice->searchDistinctDests();
$mf = new MyFormatter();


  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'voicesummary-grid',
	'dataProvider'=>$tvprovider,
    'formatter'=> $mf->setDecimals(2),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Invoice items',
	'columns'=>array(
        array('name'=>'networkname','footer'=>'Grand Total'),
        'ratingclass.code:text:Destination prefix',
        array('name'=>$model->getTotalDurationColumn(),'header'=>$model->getDurationTitle(),
            'cssClassExpression'=>'"rightalign"',
            'footer'=>$totalDuration,
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;'),
        ),
        array(
            'name'=>'ratingclass.retailhkd',
            'type'=>'number',
            'cssClassExpression'=>'"rightalign"',
            'header'=>'Rate ('.$currency.'/min)',

        ),
        array('name'=>'totalcharge','header'=>'Charge ('.$currency.')',
            'type'=>'number',
            'footer'=>number_format($totalcharge,2),
            'cssClassExpression'=>'"rightalign"',
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;'),

        ),

	),
)); ?>
Please examine this invoice immediately if no discrepancy is reported within 30 days from date of invoice, the
amount will be considered as correct. <br>
Please wire transfer amount in full (bank charges to be absorbed by remitter) to the following indicating our invoice
number  : <br>
<br>
<?php
if (empty($model->cust->bank_id))
$bankinfo = Bankinfo::model()->find();
else
$bankinfo = Bankinfo::model()->findByPk($model->cust->bank_id);
if (empty($bankinfo->address) && empty($bankinfo->swiftcode))
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$bankinfo,
        'htmlOptions' => array('class'=>'zz'),
        'itemTemplate' =>"<tr class=\"{class}\"><th style='text-align:left; font-weight: normal;'>{label}</th><td
    style='width:10px'>:</td><td>{value}</td></tr>\n",
        'attributes'=>array(
            'companyname',
            'bankdetail',
            'account',
        ),
    ));
else
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$bankinfo,
        'htmlOptions' => array('class'=>'zz'),
        'itemTemplate' =>"<tr class=\"{class}\"><th style='text-align:left; font-weight: normal;'>{label}</th><td
    style='width:10px'>:</td><td>{value}</td></tr>\n",
        'attributes'=>array(
            'companyname',
            'bankdetail',
            'account',
            'address',
            'swiftcode',
	),
));

?>
For and on behalf Of<br/>
TELEKOMUNIKASI INDONESIA INTERNATIONAL ( HONGKONG ) LIMITED<br>
<br/>
<br/>
<br/>


    <br>
<h2 style="page-break-before: always">INVOICE DETAILS</h2>

<?php
    foreach ($distinctDestinations as $destObj)
    {
        echo "<h3>Destination: [".$destObj->destination_number."] </h3>";
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$destObj->search(true),
	//'filter'=>$modelvoice,
    'formatter' => $mf->setDecimals(2),
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Voice Call Records',
	'columns'=>array(
		'start','stop','originating_number',array('name'=>$model->getDurationColumnName(),'cssClassExpression'=>'"rightalign"'),
		array('name'=>'rate','type'=>'number','cssClassExpression'=>'"rightalign"','header' =>'Rate ('.$currency.'/min)'),
        array('name'=>'ratedcharge','type'=>'number','cssClassExpression'=>'"rightalign"', 'header'=>'Charge ('.$currency.')'),

    ),
    )); 
    }
?>
