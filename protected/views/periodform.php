<?php
/* @var $this PeriodFormController */
/* @var $model PeriodForm */
/* @var $form CActiveForm */
if (empty($method)) $method= 'get';
if (!isset($autosubmit)) $autosubmit = true;
if ($autosubmit) $as = array('submit'=>''); else $as = array();
if (!isset($withAll)) $withAll = false;
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'period-form-periodform-form',
	'enableAjaxValidation'=>false,
	'action'=>$actionUrl,
	'method'=>$method,
	
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model,'batch_id',PeriodForm::getDropDown($withAll),$as); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

<?php
if (!empty($withCustomer))
{
    ?><div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->dropDownList($model,'cust_id',PeriodForm::getDropDownCust()); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>
    <?php
}
    ?>
    

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->