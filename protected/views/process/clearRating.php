o<?php
/* @var $this ProcessController */

$this->breadcrumbs=array(
	'Process'=>array('/process'),
	'ClearRating',
);
$this->menu=array(
	array('label'=>'Index', 'url'=>array('index')),	
	array('label'=>'Rating', 'url'=>array('doRating')),
	array('label'=>'Invoicing', 'url'=>array('doInvoice')),
	array('label'=>'Clear Rating', 'url'=>array('clearRating')),
);

?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p><?php echo $msg; ?>
</p>
<?php
$actionUrl = array('/process/clearRating');
$method = 'post';
$autosubmit = false;
$this->renderPartial('/periodform',compact('model','actionUrl','method','autosubmit'));
?>