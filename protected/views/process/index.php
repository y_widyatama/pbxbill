<?php
/* @var $this ProcessController */

$this->breadcrumbs=array(
	'Process',
);
$this->menu=array(
	array('label'=>'Index', 'url'=>array('index')),	
	array('label'=>'Rating', 'url'=>array('doRating')),
	array('label'=>'Invoicing', 'url'=>array('doInvoice')),
    array('label'=>'Mirroring', 'url'=>array('doMirror')),
	array('label'=>'Clear Rating', 'url'=>array('clearRating')),
);

?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>
