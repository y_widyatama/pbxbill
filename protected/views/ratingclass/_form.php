<?php
/* @var $this RatingclassController */
/* @var $model Ratingclass */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ratingclass-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'destinationnet'); ?>
		<?php echo $form->textField($model,'destinationnet',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'destinationnet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rateusd'); ?>
		<?php echo $form->textField($model,'rateusd',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'rateusd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wholesalehkd'); ?>
		<?php echo $form->textField($model,'wholesalehkd',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'wholesalehkd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'retailhkd'); ?>
		<?php echo $form->textField($model,'retailhkd',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'retailhkd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tariffschema_id'); ?>
		<?php echo $form->textField($model,'tariffschema_id'); ?>
		<?php echo $form->error($model,'tariffschema_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'roundmode'); ?>
		<?php echo $form->textField($model,'roundmode',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'roundmode'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->