<?php
/* @var $this RatingclassController */
/* @var $data Ratingclass */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('destinationnet')); ?>:</b>
	<?php echo CHtml::encode($data->destinationnet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rateusd')); ?>:</b>
	<?php echo CHtml::encode($data->rateusd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wholesalehkd')); ?>:</b>
	<?php echo CHtml::encode($data->wholesalehkd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('retailhkd')); ?>:</b>
	<?php echo CHtml::encode($data->retailhkd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tariffschema_id')); ?>:</b>
	<?php echo CHtml::encode($data->tariffschema_id); ?>
	<br />


</div>