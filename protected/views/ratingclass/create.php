<?php
/* @var $this RatingclassController */
/* @var $model Ratingclass */

$this->breadcrumbs=array(
	'Ratingclasses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ratingclass', 'url'=>array('index')),
	array('label'=>'Manage Ratingclass', 'url'=>array('admin')),
);
?>

<h1>Create Ratingclass</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>