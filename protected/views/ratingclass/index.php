<?php
/* @var $this RatingclassController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ratingclasses',
);

$this->menu=array(
	array('label'=>'Create Ratingclass', 'url'=>array('create')),
	array('label'=>'Manage Ratingclass', 'url'=>array('admin')),
);
?>

<h1>Ratingclasses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
