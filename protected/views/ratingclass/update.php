<?php
/* @var $this RatingclassController */
/* @var $model Ratingclass */

$this->breadcrumbs=array(
	'Ratingclasses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ratingclass', 'url'=>array('index')),
	array('label'=>'Create Ratingclass', 'url'=>array('create')),
	array('label'=>'View Ratingclass', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Ratingclass', 'url'=>array('admin')),
);
?>

<h1>Update Ratingclass <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>