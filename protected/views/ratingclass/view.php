<?php
/* @var $this RatingclassController */
/* @var $model Ratingclass */

$this->breadcrumbs=array(
	'Ratingclasses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ratingclass', 'url'=>array('index')),
	array('label'=>'Create Ratingclass', 'url'=>array('create')),
	array('label'=>'Update Ratingclass', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ratingclass', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ratingclass', 'url'=>array('admin')),
);
?>

<h1>View Ratingclass #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'destinationnet',
		'code',
		'rateusd',
		'wholesalehkd',
		'retailhkd',
		'tariffschema_id',
	),
)); ?>
