<?php
/* @var $this TariffSchemaController */
/* @var $model TariffSchema */

$this->breadcrumbs=array(
	'Tariff Schemas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TariffSchema', 'url'=>array('index')),
	array('label'=>'Manage TariffSchema', 'url'=>array('admin')),
);
?>

<h1>Create TariffSchema</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>