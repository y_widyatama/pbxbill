<?php
/* @var $this TariffSchemaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tariff Schemas',
);

$this->menu=array(
	array('label'=>'Create TariffSchema', 'url'=>array('create')),
	array('label'=>'Manage TariffSchema', 'url'=>array('admin')),
);
?>

<h1>Tariff Schemas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
