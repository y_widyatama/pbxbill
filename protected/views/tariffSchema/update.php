<?php
/* @var $this TariffSchemaController */
/* @var $model TariffSchema */

$this->breadcrumbs=array(
	'Tariff Schemas'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TariffSchema', 'url'=>array('index')),
	array('label'=>'Create TariffSchema', 'url'=>array('create')),
	array('label'=>'View TariffSchema', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TariffSchema', 'url'=>array('admin')),
);
?>

<h1>Update TariffSchema <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>