<?php
/* @var $this TariffSchemaController */
/* @var $model TariffSchema */

$this->breadcrumbs=array(
	'Tariff Schemas'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TariffSchema', 'url'=>array('index')),
	array('label'=>'Create TariffSchema', 'url'=>array('create')),
	array('label'=>'Update TariffSchema', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TariffSchema', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TariffSchema', 'url'=>array('admin')),
);
?>

<h1>View TariffSchema #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'dateref',
		'currency',
		'info',
	),
)); ?>
