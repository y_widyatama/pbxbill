<?php
/**
 * User: artikow
 * at 8/1/13 3:45 AM
 *
 */

$this->breadcrumbs=array(
    'Users'=>array('index'),
    $model->name=>array('view','id'=>$model->id),
    'Change Password',
);?>

<h1>Change Password</h1>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm',array(
        'id'=>'users-form',
        'enableAjaxValidation'=>true,'action'=>array('changePassword','id'=>$model->id)
    )); ?>

    <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <b>Password :
            <?php echo $form->passwordField($model,'passwd'); ?>
            <?php echo $form->error($model,'passwd'); ?></b><br/>
    </div>

    <div class="row">
        <b>Confirm Password :
            <?php echo $form->passwordField($model,'passwd2'); ?>
            <?php echo $form->error($model,'passwd2'); ?></b><br/>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->