<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
    array('label'=>'Change Password', 'url'=>array('changePassword','id'=>Yii::app()->user->id),'visible'=>!Yii::app()->user->checkAccess('admin')),
    array('label'=>'Change Password', 'url'=>array('changePassword','id'=>$model->id),'visible'=>Yii::app()->user->checkAccess('admin')),
);
?>

<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'pwdhash',
	),
)); ?>

<br>
<h2>Access Authority</h2>
<?php
	$modelauthassigment = new Authassignment;
	$modelauthassigment->userid = $model->id;
	$url=Yii::app()->createUrl("users/addauth/", array("userid"=>$model->id));
?>
<a href="<?php echo $url; ?>">Add Authority</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'authassignment-grid',
	'dataProvider'=>$modelauthassigment->search(),
	//'filter'=>$model,
	'columns'=>array(
		'userid',
		'itemname',
		/*
		array(
			'class'=>'CButtonColumn',
		),
		*/
		array
		(
			'header'=>'Delete',
			'class'=>'CButtonColumn',
			'template'=>'{delete}',
			'buttons'=>array
			(
				'delete' => array
				(
					'url'=>'Yii::app()->createUrl("users/delauth", array("userid"=>$data->userid, "itemname"=>$data->itemname))',
				),
			),
		),
	),
)); ?>