<?php
/* @var $this VoiceCdrController */
/* @var $model VoiceCdr */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'voice-cdr-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'originating_number'); ?>
		<?php echo $form->textField($model,'originating_number',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'originating_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'destination_number'); ?>
		<?php echo $form->textField($model,'destination_number',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'destination_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'origtrunkgroupclli'); ?>
		<?php echo $form->textField($model,'origtrunkgroupclli',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'origtrunkgroupclli'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'origtrunkgroup'); ?>
		<?php echo $form->textField($model,'origtrunkgroup',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'origtrunkgroup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ratingclass_id'); ?>
		<?php echo $form->textField($model,'ratingclass_id'); ?>
		<?php echo $form->error($model,'ratingclass_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ratedcharge'); ?>
		<?php echo $form->textField($model,'ratedcharge',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ratedcharge'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start'); ?>
		<?php echo $form->textField($model,'start'); ?>
		<?php echo $form->error($model,'start'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stop'); ?>
		<?php echo $form->textField($model,'stop'); ?>
		<?php echo $form->error($model,'stop'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->