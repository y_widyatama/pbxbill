<?php
/* @var $this VoiceCdrController */
/* @var $model VoiceCdr */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'originating_number'); ?>
		<?php echo $form->textField($model,'originating_number',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'destination_number'); ?>
		<?php echo $form->textField($model,'destination_number',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'origtrunkgroupclli'); ?>
		<?php echo $form->textField($model,'origtrunkgroupclli',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'origtrunkgroup'); ?>
		<?php echo $form->textField($model,'origtrunkgroup',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ratingclass_id'); ?>
		<?php echo $form->textField($model,'ratingclass_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ratedcharge'); ?>
		<?php echo $form->textField($model,'ratedcharge',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start'); ?>
		<?php echo $form->textField($model,'start'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stop'); ?>
		<?php echo $form->textField($model,'stop'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->