<?php
/* @var $this VoiceCdrController */
/* @var $data VoiceCdr */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_id')); ?>:</b>
	<?php echo CHtml::encode($data->cust_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('originating_number')); ?>:</b>
	<?php echo CHtml::encode($data->originating_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('destination_number')); ?>:</b>
	<?php echo CHtml::encode($data->destination_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origtrunkgroupclli')); ?>:</b>
	<?php echo CHtml::encode($data->origtrunkgroupclli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origtrunkgroup')); ?>:</b>
	<?php echo CHtml::encode($data->origtrunkgroup); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('duration')); ?>:</b>
	<?php echo CHtml::encode($data->duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ratingclass_id')); ?>:</b>
	<?php echo CHtml::encode($data->ratingclass_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ratedcharge')); ?>:</b>
	<?php echo CHtml::encode($data->ratedcharge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start')); ?>:</b>
	<?php echo CHtml::encode($data->start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stop')); ?>:</b>
	<?php echo CHtml::encode($data->stop); ?>
	<br />

	*/ ?>

</div>