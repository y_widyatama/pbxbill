<?php
/* @var $this VoiceCdrController */
/* @var $model VoiceCdr */

$this->breadcrumbs=array(
	'Voice Cdrs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VoiceCdr', 'url'=>array('index')),
	array('label'=>'Manage VoiceCdr', 'url'=>array('admin')),
);
?>

<h1>Create VoiceCdr</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>