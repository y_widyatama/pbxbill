<?php
/* @var $this VoiceCdrController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Voice Cdrs',
);

$this->menu=array(
	array('label'=>'Create VoiceCdr', 'url'=>array('create')),
	array('label'=>'Manage VoiceCdr', 'url'=>array('admin')),
);
?>

<h1>Voice Cdrs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
