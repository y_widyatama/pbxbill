<?php
/* @var $this VoiceCdrController */
/* @var $model VoiceCdr */

$this->breadcrumbs=array(
	'Voice Cdrs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List VoiceCdr', 'url'=>array('index')),
	array('label'=>'Create VoiceCdr', 'url'=>array('create')),
	array('label'=>'View VoiceCdr', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage VoiceCdr', 'url'=>array('admin')),
);
?>

<h1>Update VoiceCdr <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>