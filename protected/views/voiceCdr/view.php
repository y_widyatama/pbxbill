<?php
/* @var $this VoiceCdrController */
/* @var $model VoiceCdr */

$this->breadcrumbs=array(
	'Voice Cdrs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List VoiceCdr', 'url'=>array('index')),
	array('label'=>'Create VoiceCdr', 'url'=>array('create')),
	array('label'=>'Update VoiceCdr', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete VoiceCdr', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VoiceCdr', 'url'=>array('admin')),
);
?>

<h1>View VoiceCdr #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'batch_id',
		'cust_id',
		'originating_number',
		'destination_number',
		'origtrunkgroupclli',
		'origtrunkgroup',
		'duration',
		'rate',
		'ratingclass_id',
		'ratedcharge',
		'start',
		'stop',
	),
)); ?>
